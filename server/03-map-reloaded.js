const orders = [
    {
        customername: "Ray",
        total: 60,
        delivered: true,
    },
    {
        customername: "Vicky",
        total: 120,
        delivered: false,
    },
    {
        customername: "Ondriel",
        total: 180,
        delivered: true,
    },
    {
        customername: "Ramon",
        total: 240,
        delivered: true,
    },
];

const res = orders.map(item => item.total);
console.log(res);

const res2 = orders.map(item => {
    item.tax = 0.19;
    return item;
});
console.log(res2);

const res3 = orders.map(item => {
    item.tax = 0.19;
    return {
        ...item,
        tax: 0.2,
    };
});
console.log(res3);
