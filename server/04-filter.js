const words = ["spray", "limit", "elite", "exuberant"];

const newArray = [];
for (let index = 0; index < words.length; index++) {
    const item = words[index];
    if (item.length >= 6) {
        newArray.push(item);
    }
}
console.log(newArray);

const res = words.filter(item => item.length >= 6);
console.log(res);

const orders = [
    {
        customername: "Ray",
        total: 60,
        delivered: true,
    },
    {
        customername: "Vicky",
        total: 120,
        delivered: false,
    },
    {
        customername: "Ondriel",
        total: 180,
        delivered: true,
    },
    {
        customername: "Ramon",
        total: 240,
        delivered: true,
    },
];

const res3 = orders.filter(item => item.delivered && item.total >= 100);
console.log(res3);

const search = query => {
    return orders.filter(item => {
        return item.customername.includes(query);
    });
};
console.log(search("R"));
