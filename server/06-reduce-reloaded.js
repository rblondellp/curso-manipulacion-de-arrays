const items = [1, 3, 2, 3];

const res = items.reduce((obj, item) => {
    if (!obj[item]) {
        obj[item] = 1;
    } else {
        obj[item] = obj[item] + 1;
    }

    return obj;
}, {});

console.log(res);

const data = [
    { name: "Ray", level: "low" },
    { name: "Vicky", level: "medium" },
    { name: "Ramon", level: "hight" },
    { name: "Rosario", level: "hight" },
];

const res2 = data
    .map(item => item.level)
    .reduce((obj, item) => {
        if (!obj[item]) {
            obj[item] = 1;
        } else {
            obj[item] = obj[item] + 1;
        }

        return obj;
    }, {});
console.log(res2);
