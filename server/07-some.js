const numbers = [1, 2, 3, 4];

let res = false;
for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    if (element % 2 === 0) {
        res = true;
        break;
    }
}
console.log(res);

const res2 = numbers.some(item => item % 2 === 0);
console.log(res2);

const orders = [
    {
        customername: "Ray",
        total: 60,
        delivered: true,
    },
    {
        customername: "Vicky",
        total: 120,
        delivered: false,
    },
    {
        customername: "Ondriel",
        total: 180,
        delivered: true,
    },
    {
        customername: "Ramon",
        total: 240,
        delivered: true,
    },
];

const res3 = orders.some(item => item.delivered);
console.log("res3", res3);

const dates = [
    {
        startDate: new Date(2022, 12, 26, 8),
        endDate: new Date(2022, 12, 26, 17),
        title: "Trabajo",
    },
    {
        startDate: new Date(2023, 1, 1, 8),
        endDate: new Date(2022, 1, 2, 18),
        title: "Vacaciones",
    },
];

const newAppointment = {
    startDate: new Date(2023, 1, 1, 10),
    endDate: new Date(2023, 1, 1, 15),
};

const areIntervalsOverlapping = require("date-fns/areIntervalsOverlapping");

const isOverlap = newDate => {
    return dates.some(date => {
        return areIntervalsOverlapping(
            {
                start: date.startDate,
                end: date.endDate,
            },
            {
                start: newDate.startDate,
                end: newDate.endDate,
            }
        );
    });
};

console.log(isOverlap(newAppointment));
