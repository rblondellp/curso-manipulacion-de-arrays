const numbers = [1, 30, 49, 29, 10, 13];

let res = true;
for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    if (element >= 40) {
        res = false;
    }
}
console.log(res);

const res2 = numbers.every(item => item <= 40);
console.log(res2);
