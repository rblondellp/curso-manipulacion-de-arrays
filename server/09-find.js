const numbers = [1, 30, 49, 29, 10, 13];

let res = undefined;
for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    if (element === 30) {
        res = element;
        break;
    }
}
console.log(res);

const res2 = numbers.find(item => item === 30);
console.log(res2);
