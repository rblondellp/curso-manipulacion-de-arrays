const elements = ["fire", "air", "water"];

let resFinal = "";
const separator = "--";
for (let index = 0; index < elements.length; index++) {
    const element = elements[index];
    resFinal = resFinal + element + separator;
}
console.log(resFinal);

const res2 = elements.join("--");
console.log(res2);

const title = "Curso de manipulacion de arrays";
const titleFinal = title.split(" ").join("-").toLowerCase();
console.log(titleFinal);
